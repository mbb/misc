# 4 Questions Uber's Open Source Program Office answers with data

It's been said that _"Software is eating the World"_, so every company will become
a _"Software Company"_ at some point. Since open source is clearly becoming the 
mainstream path for developing software nowadays, how companies manage their 
relationship with the open source projects they depend on is going to be crucial
for their success.

Open Source Program Office (OSPO)is the company's asset to manage such relationship,
and more and more companies are setting their own offices to deal with that. Even
_the Linux Foundation_ has a project called [TODO Group](https://todogroup.org/)
focus on _"to collaborate on practices, tools, and other ways to run successful and
effective open source projects and programs"_.

Uber is one of TODO Group members and [its OSPO](https://uber.github.io/) is 
working openly with [Bitergia](https://bitergia.com) to highlight their open 
source community engagement and to solve questions regarding project activity and
performance, two important factors for decision making. Bitergia is one of the 
core contributors to [CHAOSS (Community Health Analytics
for Open Source Software)](https://chaoss.community), another Linux Foundation
project focus on:

* Establish standard implementation-agnostic metrics for measuring community activity, contributions, and health
* Produce integrated open source software for analyzing software community development
* Build reproducible project health reports/containers

Some questions that an OSPO might solve with data are:

## Where are my contributors?

From a geographical point of view, since big part of the contributions to Uber's
open source projects is happeing in GitHub, it's possible to plot them in a heat
map:

![Contributors heatmap](assets/heat-map-uber-bitergia-analytics.png)

But, there are other ways to check the geographical diversity, specially when 
contributions come from tools that don't have geo-data information, like Git or 
mailing lists. In those cases, timezones data can be a valuable resource:

![Contributors and contributions by timezone](assets/commits-authors-uber.jpg)

These charts clearly show that Uber’s OSS projects have mainly contributions 
from USA West Coast, but the contributors are worldwide distributed.

## How many core, regular and casual contributors are in my community?

Many people know about the [Bus factor](https://en.wikipedia.org/wiki/Bus_factor),
and since it can be  hard to measure, one approach is to identify who the core, 
regular and casual project contributors are, based on people’s activity:

* Core contributors: those whot have done 80% of the contributions for a certain period of time
* Regular contributors: those who have done 15% of the contributions for a certain period of time
* Casual contributors: those who have done 5% of the contributions for a certain period of time

This specific way to target community members is called 
[the onion analysis](https://blog.bitergia.com/2018/06/19/the-onion-model-analyzing-community-structure/),
and by contributions you can be talking about commits, issues submitted, pull 
requests submitted, etc.

![Onion analysis for Git](assets/core-regural-and-casual-developers.png)

You can also see the evolution over time by quarter of those numbers:

![Evolution over time of core, regular and casual contributors](assets/uber-community-onion-git.jpg)

## Is my community growing?

By tracking the number of active contributors and repositories, forums, mailing
lists, etc. over a period of time, we can see the evolution of Uber open source
community. But we can go one step beyond this metric by also identifying how 
those developers collaborate.

For example comparing Uber’s 2014-2015 projects and contributors network:

![2014-2015 repositories and authors network](assets/network-authors-repositories-2014-2015.png)

with 2017-2018 network, we can visually check how rich and complex Uber’s OSS
ecosystem is becoming.

![2017-2018 repositories and authors network](assets/network-authors-repositories-2017-2018.png)

## How am I dealing with external contributors?

Fair play is key for any open source community. How Uber deals with issues, 
questions, and code contributions from outside the company in its open source
projects shows how _welcoming_ they are, and it helps to adjust policies to
improve mentoring, documentation, etc.

For example, the median to close issues from non Uber employees during last year
has been between 4 and 5 days, while it was almost 9 days the year before.

![Uber GitHub Pull Requests management efficiency](assets/uber-efficiency.jpg)

If we look at last year GitHub Pull Requests, those submitted by Uber employees 
are closed in around 5 hours (median), while those from non Uber employees are
closed in almost a day. Is it good? It looks good for me, but it depends on Uber
OSPO goals and policies to decide if they want to improve it or not.

## Disclaimer and next steps

Every chart and data shown in this post has been created with
[GrimoireLab](https://chaoss.github.io/grimoirelab), one of CHAOSS tools, by 
analyzing all the projects managed by Uber's OSPO. There is a 
[live dashboard](https://uber.biterg.io) available to play with the data. Since 
everything is free, open source software, you can go a build your own one.

These projects are the ones released by Uber in GitHub, but are those the 
the only ones that OSPO should be worried about? Definetely not, so Uber and 
Bitergia are working on extending these analysis to the whole set of projects
where Uber is contributing to.

This concept of company's footprint in open source or _company's ecosystem_ is 
one of the topics of 
[Brian Hsieh and Manrique Lopez's talk in Scale17x](https://www.socallinuxexpo.org/scale/17x/presentations/building-collaborative-open-source-program-uber),
so, if you are around, don't miss it!

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.