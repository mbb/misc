#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys 

import requests

from perceval.backends.core.git import Git

#import numpy as np
import pandas as pd

from threading import Thread

import time
from queue import Queue

import logging
logging.basicConfig(level=logging.INFO)

import argparse
import configparser

class GitData:
    def __init__(self):
        """GitData init function
        Args:
            repo (str): Git repository URI
        Returns:
            __main__.GitData : GitData object
        """
        self.dataframe = pd.DataFrame()


    def git_repo_data(self, gitURI):
        """Function to get commits data from a git repository
        
        Args:
            gitURI (str): git repository URI
        
        Returns:
            pandas.Dataframe : Dataframe with git author, commit hash, 
                            repository name, and author commit date
        """
        try:
            r = requests.head(gitURI)
        except:
            logging.info('Error requesting %s' % gitURI)
            return None
        
        git_repo = gitURI.split('/')[-1]
        
        data_repository = Git(uri=gitURI, gitpath='/tmp/{}'.format(git_repo))
            
        df = pd.DataFrame()
        
        for commit in data_repository.fetch():
            df = df.append({'author': commit['data']['Author'],
                        'commit': commit['data']['commit'],
                        'repository': git_repo,
                        'date': commit['data']['AuthorDate']
                        },
                        ignore_index=True)

        return df
    
    def get_data(self, repositories):
        """Function to get commits data from a set of git repositories
        
        Args:
            repositories (list): List of git repositories URIs
        
        Returns:
            pandas.Dataframe : Dataframe with git author, commit hash, 
                            repository name, and author commit date
        """
        dataframes = []

        threads = []

        que = Queue()

        for i, repo in enumerate(repositories):
            t = Thread(target=lambda q, arg1: q.put(self.git_repo_data(arg1)), args=(que, repo))
            threads.append(t)
            t.start()
            logging.info('Adding {} of {} repositories to the qeue: {}'.format(i+1, len(repositories), repo))

        for t in threads:
            t.join()
        i = 0
        while not que.empty():
            i = i + 1
            result = que.get()
            dataframes.append(result)
        
        logging.info('Data from {} repositories gathered'.format(len(dataframes)))

        git_data = pd.concat(dataframes)
        
        git_data['date'] = pd.to_datetime(git_data['date'])

        self.dataframe = git_data
        
        return self.dataframe

    def parse_args(self, args):
        parser = argparse.ArgumentParser(description='Tool to extract git commit authors from a set of repositories')
        parser.add_argument('-g', '--git', dest='repo', help='Git repository URI')
        parser.add_argument('-gh', '--github', dest='github', help='GitHub owner (organization or user)')

        return parser.parse_args()

    def main(self, args):
        args = self.parse_args(args)
        if args.repo != None:
            repos = [args.repo]
        else:
            gh = GitHub()
            repos = gh.owner_repositories(args.github)
        
        dataframe = self.get_data(repos)

        dataframe['author'].drop_duplicates().to_csv('data.csv')
        logging.info('Authors data exported to data.csv')

class GitHub:
    def __init__(self):
        """GitHub class initializer
        Args:
            None
        Returns:

        """
        self.repositories = []
        
    def owner_repositories(self, name):
        """Function to get the list of git repositories for a given GitHub organization or user
        
        Args:
            name (str): GitHub organization or user name
        
        Returns:
            list: List of git repositories URIs
        """
        query = "org:{}".format(name)
        page = 1
        #repos = []
        
        r = requests.get('https://api.github.com/search/repositories?q={}&page={}'.format(query, page))
        items = r.json()['items']
        
        while len(items) > 0:
            time.sleep(5)
            for item in items:
                self.repositories.append(item['clone_url'])
            page += 1
            r = requests.get('https://api.github.com/search/repositories?q={}&page={}'.format(query, page))
            try:
                items = r.json()['items']
            except:
                logging.info('Error getting the repos list')
                logging.info(r.json())
        
        logging.info('{} repositories: {}'.format(len(self.repositories), self.repositories))
        return self.repositories

if __name__ == '__main__':
    g = GitData()
    g.main([sys.argv[1:]])