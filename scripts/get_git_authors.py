#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from perceval.backends.core.git import Git
#import numpy as np
import pandas as pd

import sys

class GitAuthors:
    def __init__(self, repo):
        """GitAuthors init function
        
        Args:
            repo (str): git repository URI
            
        Returns:
        """
        gitPath = '/tmp/'+repo.split('/')[-1]
        self.data_repository = Git(uri=repo, gitpath=gitPath)

    def dataframe(self):
        """Function to build a dataframe from a git repository
        
        Args:
        
        Returns:
            pandas.Dataframe : Dataframe with author, commit id,
                                author commit date
        """
        df = pd.DataFrame()

        for commit in self.data_repository.fetch():
            df = df.append({'author': commit['data']['Author'],
                            'commit': commit['data']['commit'],
                            'date': commit['data']['AuthorDate']
                            },
                            ignore_index=True)

        df['date'] = pd.to_datetime(df['date'])
        
        return df
    
if __name__ == '__main__':
    git_repo = GitAuthors(sys.argv[1])
    dataframe = git_repo.dataframe()
    dataframe['author'].drop_duplicates().to_csv('data.csv')