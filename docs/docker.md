# Basic

## Containers

Build

```
$ docker build -t NAME .
```

Run

```
$ docker run [OPTIONS] <tag>
```

Remove

Stop and remove all containers:
```
$ docker stop $(docker ps -a -q)
$ docker rm $(docker ps -a -q)

```

More info: https://www.digitalocean.com/community/tutorials/how-to-remove-docker-images-containers-and-volumes

Push to DockerHub

```
$ docker push [OPTIONS] NAME[:TAG]
```

## Images

An specific image:
```
$ docker rmi <image name> -f
```

List and remove all images:
```
$ docker images -a
$ docker rmi $(docker images -a -q)
```

## Composed infra

Start, stop, update, and remove
```
$ docker-compose up -d
$ docker-compose stop
$ docker-compose update
$ docker-compose rm
```
